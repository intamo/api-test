const Animal = require('../models/animalmodel.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name) {
        return res.status(400).send({
            message: "name be empty"
        });
    }
    // Create a Note
    const animal = new Animal({
        name: req.body.name,
        color: req.body.color || "#111"
    });
    // Save Note in the database
    animal.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Note."
            });
        });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Animal.find()
        .then(animals => {
            res.send(animals);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    Animal.findById(req.params.animalId)
        .then(animal => {
            if (!animal) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.animalId
                });
            }
            res.send(animal);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.animalId
                });
            }
            return res.status(500).send({
                message: "Error retrieving note with id " + req.params.animalId
            });
        });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body.name) {
        return res.status(400).send({
            message: "Name can not be empty"
        });
    }
    // Find note and update it with the request body
    Animal.findByIdAndUpdate(req.params.animalId, {
        name: req.body.name ,
        color: req.body.color || "#111"
    }, { new: true })
        .then(animal => {
            if (!animal) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.animalId
                });
            }
            res.send(animal);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.animalId
                });
            }
            return res.status(500).send({
                message: "Error updating note with id " + req.params.animalId
            });
        });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Animal.findByIdAndRemove(req.params.animalId)
        .then(animal => {
            if (!animal) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.animalId
                });
            }
            res.send({ message: "Note deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.animalId
                });
            }
            return res.status(500).send({
                message: "Could not delete note with id " + req.params.animalId
            });
        });
};