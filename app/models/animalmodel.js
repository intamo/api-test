const mongoose = require('mongoose');

const AnimalSchema = mongoose.Schema({
    name: String,
    color: String
}, {
        timestamps: true,
        versionKey: false
    });

module.exports = mongoose.model('Animal', AnimalSchema);