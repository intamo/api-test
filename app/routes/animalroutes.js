module.exports = (app) => {
    const animals = require('../controllers/animalcontroller.js');

    app.post('/animals', animals.create);

    app.get('/animals', animals.findAll);

    app.get('/animals/:animalId', animals.findOne);

    app.put('/animals/:animalId', animals.update);

    app.delete('/animals/:animalId', animals.delete);
}